const testObject = {
  name: "Bruce Wayne",
  age: 36,
  location: "Gotham",
};

const defaultProps = { phone: 979545677, email: "Bruce@gmail.com" };
const defaults = require("../defaults");
try {
  console.log(defaults(testObject, defaultProps));
} catch (e) {
  console.log(e.message);
}
