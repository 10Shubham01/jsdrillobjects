const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };
const mapObject = require("../mapObject");
try {
  const CbFunction = (item) => {
    return typeof item === "string" ? item.toUpperCase() : item * 2;
  };
  console.log(mapObject(testObject, CbFunction));
} catch (e) {
  console.log(e.message);
}
