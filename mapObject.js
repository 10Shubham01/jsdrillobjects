function mapObject(obj, cb) {
  if (obj && cb) {
    let newMapObject = {};
    for (const key in obj) {
      newMapObject[key] = cb(obj[key]);
    }
    return newMapObject;
  }
}
module.exports = mapObject;
