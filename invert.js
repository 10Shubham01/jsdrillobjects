function invert(obj) {
  if (obj) {
    let invertObj = {};
    for (const key in obj) {
      invertObj[obj[key]] = key;
    }
    return invertObj;
  }
}
module.exports = invert;
