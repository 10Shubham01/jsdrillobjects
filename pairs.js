function pairs(obj) {
  if (obj) {
    return Object.keys(obj).map((key) => [key, obj[key]]);
  }
}
module.exports = pairs;
