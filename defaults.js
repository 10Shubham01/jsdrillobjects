function defaults(obj, defaultProps) {
  if (obj && defaultProps) {
    for (const key in defaultProps) {
      if (typeof obj[key] === "undefined") {
        obj[key] = defaultProps[key];
      }
    }
    return obj;
  }
}
module.exports = defaults;
